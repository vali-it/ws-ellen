package framework;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public class ControllerResponse {

    public static Map<String, Method> getMethods() {
        Map<String, Method> methodMap = new HashMap();

        Method[] methods = BusinessLogicController.class.getDeclaredMethods();

        for (Method method : methods) {
            WebPath annotation = method.getAnnotation(WebPath.class);
            methodMap.put(annotation.path(), method);
        }
        return methodMap;
    }

    public static byte[] getResource(String path, Map<String, String> headers) throws InvocationTargetException, IllegalAccessException {

        Map<String, Method> methodMap = getMethods();
        Method controllerMethod = null;
        for (Map.Entry<String, Method> entry : methodMap.entrySet()) {
            if (path.startsWith(entry.getKey())) {
                controllerMethod = entry.getValue();
            }
        }
        if (controllerMethod != null) {
            Object invokeMethod = controllerMethod.invoke(new BusinessLogicController(),
                    "", path, headers);

            return (byte[]) invokeMethod;

        } else {
            return Response.errorNotFound();
        }
    }

}


