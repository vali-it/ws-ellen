package framework;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;

public class BusinessLogicController {

    @WebPath(path = "/hello")
    public byte[] printHello(String requestBody, String path, Map<String, String> headers) {
        StringBuilder rb = new StringBuilder();
        rb.append("HTTP/1.1 200 OK \n");
        rb.append("ContentType: text/html\n");
        rb.append("\n\n");

        rb.append("Hello World. Timestamp is ")
                .append(System.currentTimeMillis())
                .append("and your browser is").append(
                        headers.get("User-Agent")
                );

        return rb.toString().getBytes(StandardCharsets.UTF_8);
    }

    @WebPath(path = "/static-content")

    public byte[] staticContent(String requestBody, String path, Map<String, String> headers) throws IOException {

        String[] pathAsArray = path.split("/");
        if(pathAsArray.length == 2) {
            return Response.errorNotFound();

        }

        String fileName = pathAsArray[2];
        String contentType = Files.probeContentType(Path.of(fileName));
        StringBuilder rb = new StringBuilder();

        if (Files.exists(Path.of(fileName))) {

            rb.append("HTTP/1.1 200 OK \n");
            rb.append("ContentType: " + contentType);
            rb.append("\n\n");
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            baos.write(rb.toString().getBytes(StandardCharsets.UTF_8));
            baos.write(Files.readAllBytes(Path.of(fileName)));


            return baos.toByteArray();

        } else {
            return Response.errorNotFound();
        }
    }

    @WebPath(path = "/favicon.ico")
    public byte[] favicon(String requestBody, String path, Map<String, String> headers) {

        return new byte[]{};
    }

    @WebPath(path = "/foo")
    public byte[] foo(String requestBody, String path, Map<String, String> headers) {

        StringBuilder rb = new StringBuilder();
        rb.append("HTTP/1.1 200 OK \n");
        rb.append("ContentType: text/html\n");
        rb.append("\n\n");

        rb.append("Foo. Timestamp is ")
                .append(System.currentTimeMillis())
                .append("and your browser is").append(
                        headers.get("User-Agent")
                );

        return rb.toString().getBytes(StandardCharsets.UTF_8);
    }

}

