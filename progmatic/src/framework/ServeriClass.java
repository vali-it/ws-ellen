package framework;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class ServeriClass {

    public static void main(String[] args) throws IOException {

        startServer();
    }

    public static void startServer() throws IOException {

        ServerSocket serverSocket = new ServerSocket(8080);
        while (true) {
            try (Socket clientConnection = serverSocket.accept()) {
                handleClientConnection(clientConnection);
            } catch (InvocationTargetException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    private static void handleClientConnection(Socket clientConnection) throws IOException, InvocationTargetException, IllegalAccessException {

        BufferedReader br = new BufferedReader(new InputStreamReader(clientConnection.getInputStream()));

        StringBuilder requestBuilder = new StringBuilder();
        String line = br.readLine();
        String request = requestBuilder.toString();
        String response = null;
//        while (line!=null) {
//            requestBuilder.append(line + "\n");
//            line = br.readLine();
//            String[] requestArray = request.split("\n");
//            for (int i = 0; i < requestArray.length; i++){
//                if(requestArray[i].equals("")) {
//                    response=requestArray[i+1];
//                    System.out.println(response);
//                    break;
//                }
//            }
//
//        }

        boolean headersFinished = false;
        while (line != null) {
            requestBuilder.append(line + "\n");
            line = br.readLine();
            if (line.equals("")) {
                headersFinished = true;
                line = br.readLine();
                response = line;
                System.out.println(response);
                break;

            }
        }

            if (request.isBlank()) {
                return;
            }
            String[] requestLines = request.split("\n");
            String[] requestLine = requestLines[0].split(" ");
            String path = requestLine[0]; // 1


            Map<String, String> headers = new HashMap<>();

            for (int h = 2; h < requestLines.length; h++) {

                String[] headerLine = requestLines[h].split(":");
                String headerHead = headerLine[0];
                String headerContent = headerLine[1];
                headers.put(headerHead, headerContent);

            }

            responseToBrowser(clientConnection, path, headers);

        }


    public static void responseToBrowser(Socket clientConnection, String path, Map<String, String> headers) throws IOException, InvocationTargetException, IllegalAccessException {

        OutputStream clientOutput = clientConnection.getOutputStream();
        clientOutput.write(ControllerResponse.getResource(path, headers));
    }
}




