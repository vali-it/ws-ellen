package framework;

import java.nio.charset.StandardCharsets;

public class Response {
    public static byte[] errorNotFound() {
        StringBuilder error = new StringBuilder();
        error.append("HTTP/1.1 404 Not Found \n");
        error.append("ContentType: text/html\n");
        error.append("\n\n");
        error.append("404 Not Found");

        return error.toString().getBytes(StandardCharsets.UTF_8);
    }
}
